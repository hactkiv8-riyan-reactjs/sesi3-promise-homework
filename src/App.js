const arrayOfWords = ['cucumber', 'tomatos', 'avocado'];
const complicatedArray = ['cucumber', 44, true];

makeAllCaps(arrayOfWords)
    .then(sortOfWords)
    .then((result) => console.log(result))
    .catch((error) => console.log('Error Handled ' + error));


makeAllCaps(complicatedArray)
    .then(sortOfWords)
    .then((result) => console.log(result))
    .catch((error) => console.log('Error Handled ' + error));

