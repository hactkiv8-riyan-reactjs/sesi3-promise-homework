const makeAllCaps = (data = []) => {
    return new Promise((resolve, reject) => {
        try {
            resolve(data.map((val) => val.toUpperCase()));
        } catch (ex) {
            reject(ex);
        }
    });
};

const sortOfWords = (data = []) => {
    return new Promise((resolve, reject) => {
        try {
            resolve(data.sort());
        } catch (ex) {
            reject(ex);
        }
    })
};
